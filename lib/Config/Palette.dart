// Code written by Oladeji Sanyaolu (11/9/2021)

import 'package:flutter/material.dart';

class Palette {
  static const Color appPrimary = Color(0xFF075E54);
  static const Color appAccent = Color(0xFF128C7E);
  static const Color newIconColor = Color(0xFF25D366);
  static const Color ownCardColor = Color(0xffdcf8c6);
  static const Color painterColor = Color(0xff21bfa6);
}
