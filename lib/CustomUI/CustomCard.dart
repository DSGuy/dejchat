// Code written by Oladeji Sanyaolu (13/9/2021)

import 'package:dejchat/Screens/Screens.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:dejchat/Model/Model.dart';

class CustomCard extends StatelessWidget {
  final ChatModel chatModel;
  final ChatModel sourcechat;
  const CustomCard(
      {Key? key, required this.chatModel, required this.sourcechat})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => IndividualPage(
                      chatModel: chatModel,
                      sourcechat: sourcechat,
                    )));
      },
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              radius: 25,
              child: SvgPicture.asset(
                  chatModel.isGroup ? "assets/groups.svg" : "assets/person.svg",
                  color: Colors.white10,
                  height: 37,
                  width: 37),
            ),
            title: Text(
              chatModel.name,
              style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
            ),
            subtitle: Row(
              children: <Widget>[
                Icon(Icons.done_all),
                SizedBox(width: 3),
                Text(chatModel.currentMessage as String,
                    style: const TextStyle(fontSize: 13)),
              ],
            ),
            trailing: Text(chatModel.time as String),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 20.0, left: 80.0),
            child: const Divider(thickness: 1),
          ),
        ],
      ),
    );
  }
}
