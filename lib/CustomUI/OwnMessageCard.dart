// Code written by Oladeji Sanyaolu (22/9/2021)

import 'package:flutter/material.dart';
import 'package:dejchat/Config/Palette.dart';

class OwnMessageCard extends StatelessWidget {
  final String message;
  final String time;

  const OwnMessageCard({Key? key, required this.message, required this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: ConstrainedBox(
        constraints: BoxConstraints(
          maxWidth: MediaQuery.of(context).size.width - 45,
        ),
        child: Card(
            elevation: 1,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            color: Palette.ownCardColor,
            margin: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    left: 10.0,
                    right: 60.0,
                    top: 10.0,
                    bottom: 20.0,
                  ),
                  child: Text(
                    message,
                    style: const TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 4.0,
                  right: 10.0,
                  child: Row(
                    children: <Widget>[
                      Text(
                        time,
                        style: TextStyle(
                          fontSize: 13.0,
                          color: Colors.grey[600],
                        ),
                      ),
                      const SizedBox(width: 5.0),
                      const Icon(Icons.done_all, size: 20),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
