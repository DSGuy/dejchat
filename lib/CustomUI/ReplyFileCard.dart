// Code written by Oladeji Sanyaolu (06/10/2021)

import 'package:flutter/material.dart';

class ReplyFileCard extends StatelessWidget {
  final String filepath;
  final String? message;
  final String time;
  static String serverUrl = "https://desolate-savannah-30852.herokuapp.com";
  const ReplyFileCard(
      {Key? key, required this.filepath, this.message, required this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 5.0),
        child: Container(
          height: MediaQuery.of(context).size.height / 2.3,
          width: MediaQuery.of(context).size.width / 1.8,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15.0),
            color: Colors.grey[400],
          ),
          child: Card(
            margin: const EdgeInsets.all(3.0),
            color: Colors.teal[400],
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                    child: Image.network("$serverUrl/uploads/$filepath",
                        fit: BoxFit.fitHeight)),
                message!.length > 0
                    ? Container(
                        height: 40,
                        padding: const EdgeInsets.only(left: 15.0, top: 8.0),
                        child: Text(
                          message!,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 15.0,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
