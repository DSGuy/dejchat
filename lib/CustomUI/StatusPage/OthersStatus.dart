// Code written by Oladeji Sanyaolu (27/9/2021)

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:dejchat/Config/Palette.dart';

class OthersStatus extends StatelessWidget {
  final String name;
  final String time;
  final String imageName;
  final bool isSeen;
  final int statusNum;

  const OthersStatus(
      {Key? key,
      required this.name,
      required this.time,
      required this.imageName,
      required this.isSeen,
      required this.statusNum})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: CustomPaint(
        painter: StatusPainter(isSeen: isSeen, statusNum: statusNum),
        child: CircleAvatar(
          radius: 26.0,
          backgroundImage: AssetImage(imageName),
        ),
      ),
      title: Text(
        name,
        style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
      ),
      subtitle: Text(
        "Today at, $time",
        style: TextStyle(fontSize: 14.0, color: Colors.grey[900]),
      ),
    );
  }
}

degreeToRadian(double degree) {
  return degree * pi / 180;
}

class StatusPainter extends CustomPainter {
  bool isSeen;
  int statusNum;

  StatusPainter({required this.isSeen, required this.statusNum});

  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..isAntiAlias = true
      ..strokeWidth = 6.0
      ..color = isSeen ? Colors.grey : Palette.painterColor
      ..style = PaintingStyle.stroke;
    drawArc(canvas, size, paint);
  }

  void drawArc(Canvas canvas, Size size, Paint paint) {
    if (statusNum == 1) {
      canvas.drawArc(Rect.fromLTWH(0.0, 0.0, size.width, size.height),
          degreeToRadian(0), degreeToRadian(330), false, paint);
    } else {
      double degree = -90.0;
      double arc = 360 / statusNum;
      for (int i = 0; i < statusNum; i++) {
        canvas.drawArc(Rect.fromLTWH(0.0, 0.0, size.width, size.height),
            degreeToRadian(degree + 4), degreeToRadian(arc - 8), false, paint);
      }
      degree += arc;
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
