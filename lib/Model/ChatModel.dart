// Code written by Oladeji Sanyaolu (13/9/2021)

class ChatModel {
  String name;
  String? icon;
  bool isGroup;
  String? time;
  String? currentMessage;
  String? status;
  bool? select;
  int id;

  ChatModel({
    required this.name,
    this.icon,
    required this.isGroup,
    this.time,
    this.currentMessage,
    this.status,
    this.select = false,
    required this.id,
  });
}
