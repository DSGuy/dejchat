// Code written by Oladeji Sanyaolu (03/10/2021)

class CountryModel {
  String name;
  String code;
  String flag;

  CountryModel({required this.name, required this.code, required this.flag});
}
