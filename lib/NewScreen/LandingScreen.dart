// Code written by Oladeji Sanyaolu (03/10/2021)

import 'package:dejchat/NewScreen/NewScreen.dart';
import 'package:flutter/material.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              const SizedBox(height: 50),
              Text(
                "Welcome to Dejchat",
                style: TextStyle(
                  color: Colors.teal,
                  fontSize: 29,
                  fontWeight: FontWeight.w600,
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 8),
              Image.asset(
                "assets/bg.png",
                color: Colors.greenAccent[700],
                height: 340,
                width: 340,
              ),
              SizedBox(height: MediaQuery.of(context).size.height / 9),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 17.0,
                    ),
                    children: <InlineSpan>[
                      TextSpan(
                        text: "Agree and Continue to accept the ",
                        style: TextStyle(color: Colors.grey[600]),
                      ),
                      TextSpan(
                        text: "Dejchat Terms of Service and Privacy Policy",
                        style: const TextStyle(color: Colors.cyan),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(height: 20),
              InkWell(
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (builder) => LoginPage()),
                      (route) => false);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width - 110.0,
                  height: 50,
                  child: Card(
                    margin: const EdgeInsets.all(0.0),
                    elevation: 8.0,
                    color: Colors.greenAccent[700],
                    child: Center(
                      child: Text(
                        "AGREE & CONTINUE",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w600,
                            fontSize: 18.0),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
