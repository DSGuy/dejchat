// Code written by Oladeji Sanyaolu (03/10/2021)

import 'package:dejchat/Model/Model.dart';
import 'package:dejchat/NewScreen/NewScreen.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String countryname = "Nigeria";
  String countrycode = "+234";
  TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: const Text(
          "Enter your phone number",
          style: TextStyle(
            color: Colors.teal,
            fontWeight: FontWeight.w700,
            wordSpacing: 1,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          const Icon(Icons.more_vert, color: Colors.black),
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: <Widget>[
            const Text(
              "Dejchat will send an SMS message to verify your number",
              style: TextStyle(fontSize: 13.5),
            ),
            const SizedBox(height: 5.0),
            Text(
              "What's my number?",
              style: TextStyle(
                fontSize: 12.8,
                color: Colors.cyan[800],
              ),
            ),
            const SizedBox(height: 15.0),
            countryCard(),
            const SizedBox(height: 15.0),
            number(),
            Expanded(child: Container()),
            InkWell(
              onTap: () {
                if (_textEditingController.text.length < 10) {
                  showNoNumberDilogue();
                } else {
                  showMyDilogue();
                }
              },
              child: Container(
                color: Colors.tealAccent[400],
                height: 40.0,
                width: 70.0,
                child: Center(
                  child: Text(
                    "NEXT",
                    style: const TextStyle(fontWeight: FontWeight.w600),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 40.0),
          ],
        ),
      ),
    );
  }

  Widget countryCard() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (builder) => CountryPage(
                      setCountryData: setCountryData,
                    )));
      },
      child: Container(
        width: MediaQuery.of(context).size.width / 1.5,
        padding: const EdgeInsets.symmetric(vertical: 5.0),
        decoration: const BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: Colors.teal,
              width: 1.8,
            ),
          ),
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                child: Center(
                  child: Text(
                    countryname,
                    style: const TextStyle(fontSize: 16),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget number() {
    return Container(
      width: MediaQuery.of(context).size.width / 1.5,
      height: 38,
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      child: Row(
        children: <Widget>[
          Container(
            width: 70.0,
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.teal,
                  width: 1.8,
                ),
              ),
            ),
            child: Row(
              children: <Widget>[
                const SizedBox(width: 10),
                const Text(
                  "+",
                  style: TextStyle(fontSize: 18.0),
                ),
                const SizedBox(width: 20.0),
                Text(
                  countrycode.substring(1),
                  style: TextStyle(fontSize: 15.0),
                ),
              ],
            ),
          ),
          const SizedBox(width: 30.0),
          Container(
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Colors.teal,
                  width: 1.8,
                ),
              ),
            ),
            width: MediaQuery.of(context).size.width / 1.5 - 100,
            child: TextFormField(
              controller: _textEditingController,
              keyboardType: TextInputType.number,
              decoration: const InputDecoration(
                border: InputBorder.none,
                contentPadding: EdgeInsets.all(8.0),
                hintText: "phone number",
              ),
            ),
          ),
        ],
      ),
    );
  }

  void setCountryData(CountryModel countryModel) {
    setState(() {
      countryname = countryModel.name;
      countrycode = countryModel.code;
    });

    Navigator.pop(context);
  }

  Future<void> showMyDilogue() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                const Text("We will be verifying your phone number",
                    style: const TextStyle(fontSize: 14.0)),
                const SizedBox(height: 10),
                Text(countrycode + " " + _textEditingController.text,
                    style: const TextStyle(
                        fontSize: 14.0, fontWeight: FontWeight.w500)),
                const Text("Is this OK, or would you like to edit the number?",
                    style: const TextStyle(fontSize: 13.5))
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Edit"),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (builder) => OtpScreen(
                              countryCode: countrycode,
                              number: _textEditingController.text,
                            )));
              },
              child: Text("Ok"),
            ),
          ],
        );
      },
    );
  }

  Future<void> showNoNumberDilogue() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                const Text("There is no number you entered ",
                    style: const TextStyle(fontSize: 14.0)),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("Ok"),
            ),
          ],
        );
      },
    );
  }
}
