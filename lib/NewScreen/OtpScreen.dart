// Code written by Oladeji Sanyaolu (04/10/2021)

import 'package:flutter/material.dart';

import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class OtpScreen extends StatefulWidget {
  final String number;
  final String countryCode;

  const OtpScreen({Key? key, required this.number, required this.countryCode})
      : super(key: key);

  @override
  _OtpScreenState createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "Verify +${widget.countryCode} ${widget.number}",
          style: TextStyle(
            color: Colors.teal[800],
            fontSize: 16.5,
          ),
        ),
        actions: <Widget>[
          IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.more_vert,
              color: Colors.black,
            ),
          ),
        ],
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.symmetric(horizontal: 35.0),
        child: Column(
          children: <Widget>[
            const SizedBox(height: 10),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                children: <InlineSpan>[
                  TextSpan(
                    text: "We have sent an SMS with a code to ",
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 14.5,
                    ),
                  ),
                  TextSpan(
                    text: "+" + widget.countryCode + " " + widget.number,
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16.5,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  TextSpan(
                    text: " Wrong number?",
                    style: TextStyle(
                      color: Colors.cyan[800],
                      fontSize: 16.5,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(height: 5.0),
            OTPTextField(
              length: 6,
              width: MediaQuery.of(context).size.width,
              fieldWidth: 30,
              style: TextStyle(fontSize: 17.0),
              textFieldAlignment: MainAxisAlignment.spaceAround,
              fieldStyle: FieldStyle.underline,
              onCompleted: (pin) {
                print("Completed: " + pin);
              },
            ),
            const SizedBox(height: 20.0),
            Text(
              "Enter 6-digit code",
              style: TextStyle(
                color: Colors.grey[600],
                fontSize: 14.0,
              ),
            ),
            const SizedBox(height: 30.0),
            bottomButton("Resend SMS", Icons.message),
            const SizedBox(height: 12.0),
            const Divider(thickness: 1.5),
            bottomButton("Call Me", Icons.call),
          ],
        ),
      ),
    );
  }

  Widget bottomButton(String text, IconData icon) {
    return Row(
      children: <Widget>[
        Icon(icon, color: Colors.teal, size: 24.0),
        const SizedBox(width: 25.0),
        Text(
          text,
          style: const TextStyle(
            color: Colors.teal,
            fontSize: 14.5,
          ),
        ),
      ],
    );
  }
}
