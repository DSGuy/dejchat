// Code written by Oladeji Sanyaolu (12/9/2021)

import 'package:flutter/material.dart';
import 'package:dejchat/Screens/Screens.dart';
import 'package:dejchat/CustomUI/CustomUI.dart';
import 'package:dejchat/Model/Model.dart';

class ChatPage extends StatefulWidget {
  final List<ChatModel> chatmodels;
  final ChatModel sourcechat;

  const ChatPage({Key? key, required this.chatmodels, required this.sourcechat})
      : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (builder) => SelectContact()));
        },
        child: Icon(Icons.chat),
      ),
      body: ListView.builder(
        itemCount: widget.chatmodels.length,
        itemBuilder: (context, index) => CustomCard(
          chatModel: widget.chatmodels[index],
          sourcechat: widget.sourcechat,
        ),
      ),
    );
  }
}
