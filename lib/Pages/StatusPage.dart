// Code written by Oladeji Sanyaolu (27/9/2021)

import 'package:flutter/material.dart';
import 'package:dejchat/CustomUI/CustomUI.dart';

class StatusPage extends StatefulWidget {
  const StatusPage({Key? key}) : super(key: key);

  @override
  _StatusPageState createState() => _StatusPageState();
}

class _StatusPageState extends State<StatusPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Container(
            child: FloatingActionButton(
              backgroundColor: Colors.blueGrey[100],
              elevation: 8,
              onPressed: () {},
              child: Icon(Icons.edit, color: Colors.blueGrey[900]),
            ),
          ),
          const SizedBox(height: 13),
          FloatingActionButton(
            onPressed: () {},
            backgroundColor: Colors.greenAccent[700],
            elevation: 5,
            child: const Icon(Icons.camera_alt),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            const SizedBox(height: 10),
            HeadOwnStatus(),
            label("Recent updates"),
            OthersStatus(
              name: "Dejloaf",
              imageName: "assets/2.jpeg",
              time: "01:23",
              isSeen: true,
              statusNum: 1,
            ),
            OthersStatus(
              name: "Liz",
              imageName: "assets/3.jpg",
              time: "06:24",
              isSeen: false,
              statusNum: 1,
            ),
            OthersStatus(
              name: "Carl",
              imageName: "assets/1.jpg",
              time: "02:27",
              isSeen: false,
              statusNum: 2,
            ),
            const SizedBox(height: 10),
            label("Viewed updates"),
            OthersStatus(
              name: "Dejloaf",
              imageName: "assets/2.jpeg",
              time: "01:23",
              isSeen: true,
              statusNum: 3,
            ),
            OthersStatus(
              name: "Liz",
              imageName: "assets/3.jpg",
              time: "06:24",
              isSeen: true,
              statusNum: 4,
            ),
            OthersStatus(
              name: "Carl",
              imageName: "assets/1.jpg",
              time: "02:27",
              isSeen: true,
              statusNum: 10,
            ),
          ],
        ),
      ),
    );
  }

  Widget label(String labelname) {
    return Container(
      height: 33,
      width: MediaQuery.of(context).size.width,
      color: Colors.grey[300],
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 13.0,
          vertical: 7.0,
        ),
        child: Text(labelname,
            style: const TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.bold,
            )),
      ),
    );
  }
}
