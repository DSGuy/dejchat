// Code written by Oladeji Sanyaolu (07/10/2021)

import 'package:flutter/material.dart';

class CallScreen extends StatefulWidget {
  const CallScreen({Key? key}) : super(key: key);

  @override
  _CallScreenState createState() => _CallScreenState();
}

class _CallScreenState extends State<CallScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          callCard("Dejloaf", Icons.call_made, Colors.green, "18 July, 18:35"),
        ],
      ),
    );
  }

  Widget callCard(
      String name, IconData iconData, Color iconColor, String time) {
    return Card(
      margin: const EdgeInsets.only(bottom: 0.5),
      child: ListTile(
        leading: CircleAvatar(),
        title: Text(
          name,
          style: const TextStyle(
            fontWeight: FontWeight.w500,
          ),
        ),
        subtitle: Row(
          children: <Widget>[
            Icon(
              iconData,
              color: iconColor,
              size: 20.0,
            ),
            const SizedBox(width: 6.0),
            Text(time, style: const TextStyle(fontSize: 12.8)),
          ],
        ),
        trailing: const Icon(
          Icons.call,
          size: 26,
          color: Colors.teal,
        ),
      ),
    );
  }
}
