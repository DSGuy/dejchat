// Code written by Oladeji Sanyaolu (17/9/2021)

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:dejchat/Screens/Screens.dart';
import 'package:camera/camera.dart';
//import 'package:path/path.dart';
//import 'package:path_provider/path_provider.dart';

List<CameraDescription>? cameras;

class CameraScreen extends StatefulWidget {
  final Function? onImageSend;
  const CameraScreen({Key? key, required this.onImageSend}) : super(key: key);

  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {
  late CameraController _cameraController;
  late Future<void> cameraValue;
  bool isRecording = false;
  bool flashOn = false;
  bool isCameraFront = true;
  double flipCamTransform = 0;

  @override
  void initState() {
    super.initState();
    _cameraController = CameraController(cameras![0], ResolutionPreset.high);
    cameraValue = _cameraController.initialize();
  }

  @override
  void dispose() {
    super.dispose();
    _cameraController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          FutureBuilder(
              future: cameraValue,
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: CameraPreview(_cameraController));
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              }),
          Positioned(
            bottom: 0.0,
            child: Container(
              color: Colors.black,
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          flashOn ? Icons.flash_on : Icons.flash_off,
                          color: Colors.white,
                          size: 28,
                        ),
                        onPressed: () {
                          setState(() {
                            flashOn = !flashOn;
                          });
                          flashOn
                              ? _cameraController.setFlashMode(FlashMode.torch)
                              : _cameraController.setFlashMode(FlashMode.off);
                        },
                      ),
                      GestureDetector(
                        onLongPress: () async {
                          await _cameraController.startVideoRecording().then(
                            (_) {
                              if (mounted)
                                setState(() {
                                  isRecording = true;
                                });
                            },
                          );
                        },
                        onLongPressUp: () async {
                          /*
                          await _cameraController.stopVideoRecording().then(
                            (XFile? file) {
                              if (mounted)
                                setState(() {
                                  isRecording = false;
                                });
                            },
                          );
                          */
                          XFile videopath =
                              await _cameraController.stopVideoRecording();

                          setState(() {
                            isRecording = false;
                          });

                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (builder) =>
                                    VideoViewPage(path: videopath.path)),
                          );
                        },
                        onTap: () {
                          if (!isRecording) takePhoto(context);
                        },
                        child: isRecording
                            ? const Icon(
                                Icons.radio_button_on,
                                color: Colors.red,
                                size: 80.0,
                              )
                            : const Icon(
                                Icons.panorama_fish_eye,
                                color: Colors.white,
                                size: 70,
                              ),
                      ),
                      IconButton(
                        icon: Transform.rotate(
                          angle: flipCamTransform,
                          child: Icon(
                            Icons.flip_camera_ios,
                            color: Colors.white,
                            size: 28,
                          ),
                        ),
                        onPressed: () async {
                          setState(() {
                            isCameraFront = !isCameraFront;
                            flipCamTransform = flipCamTransform + pi;
                          });
                          int cameraPos = isCameraFront ? 0 : 1;
                          _cameraController = CameraController(
                              cameras![cameraPos], ResolutionPreset.high);
                          cameraValue = _cameraController.initialize();
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 4),
                  Text(
                    "Hold for Video, tap for photo",
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void takePhoto(BuildContext context) async {
    String? filepath;
    await _cameraController.takePicture().then((XFile? file) {
      if (mounted) {
        setState(() {
          filepath = file!.path;
        });
      }
    });
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (builder) => CameraViewPage(
          path: filepath!,
          onImageSend: widget.onImageSend!,
        ),
      ),
    );
  }
}
