// Code written by Oladeji Sanyaolu (16/9/2021)

import 'package:flutter/material.dart';

import 'package:dejchat/Model/Model.dart';
import 'package:dejchat/CustomUI/CustomUI.dart';

class CreateGroup extends StatefulWidget {
  const CreateGroup({Key? key}) : super(key: key);

  @override
  _CreateGroupState createState() => _CreateGroupState();
}

class _CreateGroupState extends State<CreateGroup> {
  List<ChatModel> contacts = [
    ChatModel(
      name: "Dejloaf",
      status: "A chad",
      isGroup: false,
      id: 1,
    ),
    ChatModel(
      name: "Liz",
      status: "Dejloaf's bestie",
      isGroup: false,
      id: 2,
    ),
    ChatModel(
      name: "Carl",
      status: "Marx",
      isGroup: false,
      id: 3,
    ),
    ChatModel(
      name: "E",
      status: "A Sports",
      isGroup: false,
      id: 4,
    ),
  ];
  List<ChatModel> groupmember = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("New Group",
                style: const TextStyle(
                  fontSize: 19,
                  fontWeight: FontWeight.bold,
                )),
            Text("Add participants",
                style: const TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                )),
          ],
        ),
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.search), onPressed: () {}),
        ],
      ),
      body: Stack(
        children: <Widget>[
          ListView.builder(
              itemCount: contacts.length + 1,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return Container(
                    height: groupmember.length > 0 ? 90 : 10,
                  );
                }
                return InkWell(
                  onTap: () {
                    if (!contacts[index - 1].select!) {
                      setState(() {
                        contacts[index - 1].select = true;
                        groupmember.add(contacts[index - 1]);
                      });
                    } else {
                      setState(() {
                        contacts[index - 1].select = false;
                        groupmember.remove(contacts[index - 1]);
                      });
                    }
                  },
                  child: ContactCard(
                    contact: contacts[index - 1],
                  ),
                );
              }),
          groupmember.length > 0
              ? Column(
                  children: <Widget>[
                    Container(
                      height: 75.0,
                      color: Colors.white,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: contacts.length,
                          itemBuilder: (context, index) {
                            if (contacts[index].select!) {
                              return InkWell(
                                  onTap: () {
                                    setState(() {
                                      groupmember.remove(contacts[index]);
                                      contacts[index].select = false;
                                    });
                                  },
                                  child: AvatarCard(contact: contacts[index]));
                            } else {
                              return const SizedBox.shrink();
                            }
                          }),
                    ),
                    const Divider(thickness: 1),
                  ],
                )
              : const SizedBox.shrink(),
        ],
      ),
    );
  }
}
