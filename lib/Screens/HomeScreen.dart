// Code written by Oladeji Sanyaolu (11/9/2021)

import 'package:flutter/material.dart';
import 'package:dejchat/Model/Model.dart';
import 'package:dejchat/Pages/Pages.dart';
import 'package:dejchat/Screens/Screens.dart';

class HomeScreen extends StatefulWidget {
  final List<ChatModel> chatmodels;
  final ChatModel sourcechat;
  const HomeScreen(
      {Key? key, required this.chatmodels, required this.sourcechat})
      : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TabController(length: 4, vsync: this, initialIndex: 1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dejchat"),
        actions: [
          IconButton(icon: Icon(Icons.search), onPressed: () {}),
          PopupMenuButton<String>(onSelected: (value) {
            print(value);
          }, itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(child: Text("New group"), value: "New group"),
              PopupMenuItem(
                  child: Text("New broadcast"), value: "New broadcast"),
              PopupMenuItem(child: Text("Dejchat Web"), value: "Dejchat Web"),
              PopupMenuItem(
                  child: Text("Starred message"), value: "Starred message"),
              PopupMenuItem(child: Text("Settings"), value: "Settings"),
            ];
          }),
        ],
        bottom: TabBar(
          controller: _controller,
          indicatorColor: Colors.white,
          tabs: [
            Tab(icon: Icon(Icons.camera_alt)),
            Tab(text: "CHATS"),
            Tab(text: "STATUS"),
            Tab(text: "CALLS"),
          ],
        ),
      ),
      body: TabBarView(
        controller: _controller,
        children: <Widget>[
          CameraPage(),
          ChatPage(
              chatmodels: widget.chatmodels, sourcechat: widget.sourcechat),
          StatusPage(),
          CallScreen(),
        ],
      ),
    );
  }
}
