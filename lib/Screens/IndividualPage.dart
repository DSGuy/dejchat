// Code written by Oladeji Sanyaolu (14/9/2021)

import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:dejchat/CustomUI/CustomUI.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:dejchat/Config/Palette.dart';
import 'package:dejchat/Model/Model.dart';
import 'package:emoji_picker_flutter/emoji_picker_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:camera/camera.dart';
import 'package:http/http.dart' as http;

import 'package:socket_io_client/socket_io_client.dart' as IO;

import 'Screens.dart';

class IndividualPage extends StatefulWidget {
  final ChatModel chatModel;
  final ChatModel sourcechat;

  const IndividualPage(
      {Key? key, required this.chatModel, required this.sourcechat})
      : super(key: key);

  @override
  _IndividualPageState createState() => _IndividualPageState();
}

class _IndividualPageState extends State<IndividualPage> {
  bool showEmojiPicker = false;
  bool sendButton = false;
  FocusNode focusNode = FocusNode();
  late IO.Socket socket;
  List<MessageModel> messages = [];
  ImagePicker _imagePicker = ImagePicker();
  XFile? imageFile;
  static String serverUrl =
      "https://desolate-savannah-30852.herokuapp.com"; // http://192.168.12.111:5000

  TextEditingController _controller = TextEditingController();
  ScrollController _scrollController = ScrollController();
  int popTime = 0;

  @override
  void initState() {
    super.initState();
    connect();
    focusNode.addListener(() {
      if (focusNode.hasFocus) {
        setState(() {
          showEmojiPicker = false;
        });
      }
    });
  }

  void connect() {
    socket = IO.io(serverUrl, <String, dynamic>{
      "transports": ["websocket"],
      "autoconnect": false,
    });
    socket.connect();
    socket.emit("signin", widget.sourcechat.id);
    socket.onConnect((data) {
      print("Connected!");
      socket.on("message", (msg) {
        print(msg);
        setMessage("destination", msg["message"], msg["path"]);
        _scrollController.animateTo(
          _scrollController.position.maxScrollExtent,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeOut,
        );
      });
    });
  }

  void sendMessage(String message, int sourceId, int targetId, String path) {
    setMessage("source", message, path);
    socket.emit("message", {
      "message": message,
      "sourceId": sourceId,
      "targetId": targetId,
      "path": path,
    });
  }

  void setMessage(String type, String message, String path) {
    MessageModel messageModel = MessageModel(
      type: type,
      message: message,
      path: path,
      time: DateTime.now().toString().substring(10, 16),
    );

    setState(() {
      setState(() {
        messages.add(messageModel);
      });
    });
  }

  void onImageSend(String path, String message) async {
    for (int i = 0; i < popTime; i++) {
      Navigator.pop(context);
    }

    setState(() {
      popTime = 0;
    });

    var request =
        http.MultipartRequest("POST", Uri.parse("$serverUrl/routes/add_image"));
    request.files.add(await http.MultipartFile.fromPath("img", path));
    request.headers.addAll({
      "Content-type": "multipart/form-data",
    });

    http.StreamedResponse response = await request.send();
    var httpResponse = await http.Response.fromStream(response);
    var data = json.decode(httpResponse.body);

    print(data['path']);
    print(response.statusCode);

    setMessage("source", message, path);
    socket.emit("message", {
      "message": message,
      "sourceId": widget.sourcechat.id,
      "targetId": widget.chatModel.id,
      "path": data['path'],
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image.asset(
          "assets/whatsapp_background.png",
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          fit: BoxFit.cover,
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          appBar: PreferredSize(
            preferredSize: const Size.fromHeight(60.0),
            child: AppBar(
              leadingWidth: 70,
              titleSpacing: 0,
              leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const Icon(Icons.arrow_back, size: 24),
                    CircleAvatar(
                      child: SvgPicture.asset(
                          widget.chatModel.isGroup
                              ? "assets/groups.svg"
                              : "assets/person.svg",
                          color: Colors.white,
                          height: 36,
                          width: 36),
                      radius: 20,
                      backgroundColor: Colors.blueGrey,
                    ),
                  ],
                ),
              ),
              title: InkWell(
                onTap: () {},
                child: Container(
                  margin: const EdgeInsets.all(5.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        widget.chatModel.name,
                        style: const TextStyle(
                          fontSize: 18.5,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "last seen today at 12:05",
                        style: const TextStyle(
                          fontSize: 13,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              actions: <Widget>[
                IconButton(
                  icon: const Icon(Icons.videocam),
                  onPressed: () {},
                ),
                IconButton(
                  icon: const Icon(Icons.call),
                  onPressed: () {},
                ),
                PopupMenuButton<String>(onSelected: (value) {
                  print(value);
                }, itemBuilder: (BuildContext context) {
                  return [
                    PopupMenuItem(
                      child: const Text("View Contact"),
                      value: "View Contact",
                    ),
                    PopupMenuItem(
                      child: const Text("Media, links, and docs"),
                      value: "Media, links, and docs",
                    ),
                    PopupMenuItem(
                      child: const Text("Dejchat Web"),
                      value: "Dejchat Web",
                    ),
                    PopupMenuItem(
                      child: const Text("Search"),
                      value: "Search",
                    ),
                    PopupMenuItem(
                      child: const Text("Mute Notification"),
                      value: "Mute Notification",
                    ),
                    PopupMenuItem(
                      child: const Text("Wallpapaer"),
                      value: "Wallpapaer",
                    ),
                  ];
                }),
              ],
            ),
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: WillPopScope(
              child: Column(
                children: <Widget>[
                  Expanded(
                    //height: MediaQuery.of(context).size.height - 140,
                    child: ListView.builder(
                      shrinkWrap: true,
                      controller: _scrollController,
                      itemCount: messages.length + 1,
                      itemBuilder: (context, index) {
                        if (index == messages.length) {
                          return Container(height: 70);
                        }
                        if (messages[index].type == "source") {
                          if (messages[index].path!.length > 0) {
                            return OwnFileCard(
                              filepath: messages[index].path as String,
                              message: messages[index].message,
                              time: messages[index].time,
                            );
                          } else {
                            return OwnMessageCard(
                              message: messages[index].message,
                              time: messages[index].time,
                            );
                          }
                        } else {
                          if (messages[index].path!.length > 0) {
                            return ReplyFileCard(
                              filepath: messages[index].path as String,
                              message: messages[index].message,
                              time: messages[index].time,
                            );
                          }
                          return ReplyCard(
                            message: messages[index].message,
                            time: messages[index].time,
                          );
                        }
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      //height: 70.0,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width - 60,
                                child: Card(
                                  margin: const EdgeInsets.only(
                                      left: 2.0, right: 2.0, bottom: 8),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  ),
                                  child: TextFormField(
                                    controller: _controller,
                                    focusNode: focusNode,
                                    textAlignVertical: TextAlignVertical.center,
                                    keyboardType: TextInputType.multiline,
                                    maxLines: 5,
                                    minLines: 1,
                                    onChanged: (value) {
                                      if (value.length > 0)
                                        setState(() {
                                          sendButton = true;
                                        });
                                      else
                                        setState(() {
                                          sendButton = false;
                                        });
                                    },
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Type a message",
                                      prefixIcon: IconButton(
                                        icon: const Icon(
                                            Icons.emoji_emotions_outlined),
                                        onPressed: () {
                                          focusNode.unfocus();
                                          focusNode.canRequestFocus = false;
                                          setState(() {
                                            showEmojiPicker = !showEmojiPicker;
                                          });
                                        },
                                      ),
                                      suffixIcon: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          IconButton(
                                            icon: const Icon(Icons.attach_file),
                                            onPressed: () {
                                              showModalBottomSheet(
                                                  backgroundColor:
                                                      Colors.transparent,
                                                  context: context,
                                                  builder: (builder) =>
                                                      _bottomSheet());
                                            },
                                          ),
                                          IconButton(
                                            icon: const Icon(Icons.camera_alt),
                                            onPressed: () {
                                              setState(() {
                                                popTime = 2;
                                              });
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                  builder: (builder) =>
                                                      CameraScreen(
                                                    onImageSend: onImageSend,
                                                  ),
                                                ),
                                              );
                                            },
                                          )
                                        ],
                                      ),
                                      contentPadding: const EdgeInsets.all(5.0),
                                    ),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                  bottom: 8.0,
                                  right: 5.0,
                                  left: 2.0,
                                ),
                                child: CircleAvatar(
                                    backgroundColor: Palette.appAccent,
                                    radius: 25.0,
                                    child: IconButton(
                                      icon: Icon(
                                          sendButton ? Icons.send : Icons.mic,
                                          color: Colors.white),
                                      onPressed: () {
                                        if (sendButton) {
                                          _scrollController.animateTo(
                                            _scrollController
                                                .position.maxScrollExtent,
                                            duration: const Duration(
                                                milliseconds: 300),
                                            curve: Curves.easeOut,
                                          );
                                        }
                                        sendMessage(
                                          _controller.text,
                                          widget.sourcechat.id,
                                          widget.chatModel.id,
                                          "",
                                        );

                                        _controller.clear();

                                        setState(() {
                                          sendButton = false;
                                        });
                                      },
                                    )),
                              ),
                            ],
                          ),
                          showEmojiPicker ? _emojiSelect() : SizedBox.shrink(),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              onWillPop: () {
                if (showEmojiPicker) {
                  setState(() {
                    showEmojiPicker = false;
                  });
                } else {
                  Navigator.pop(context);
                }
                return Future.value(false);
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget _bottomSheet() {
    return Container(
      height: 278,
      width: MediaQuery.of(context).size.width,
      child: Card(
        margin: const EdgeInsets.all(18),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _iconCreation(Icons.insert_drive_file, Colors.indigo,
                      "Documents", () {}),
                  const SizedBox(width: 40),
                  _iconCreation(Icons.camera_alt, Colors.pink, "Camera", () {
                    setState(() {
                      popTime = 3;
                    });
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (builder) => CameraScreen(
                          onImageSend: onImageSend,
                        ),
                      ),
                    );
                  }),
                  const SizedBox(width: 40),
                  _iconCreation(
                    Icons.insert_photo,
                    Colors.purple,
                    "Gallery",
                    () async {
                      setState(() {
                        popTime = 2;
                      });
                      imageFile = await _imagePicker.pickImage(
                          source: ImageSource.gallery);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (builder) => CameraViewPage(
                            path: imageFile!.path,
                            onImageSend: onImageSend,
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
              const SizedBox(height: 30.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _iconCreation(Icons.headset, Colors.orange, "Audio", () {}),
                  const SizedBox(width: 40),
                  _iconCreation(
                      Icons.location_pin, Colors.teal, "Location", () {}),
                  const SizedBox(width: 40),
                  _iconCreation(Icons.person, Colors.blue, "Contact", () {}),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _iconCreation(
      IconData icon, Color color, String text, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Column(children: <Widget>[
        CircleAvatar(
          radius: 30,
          backgroundColor: color,
          child: Icon(
            icon,
            size: 29,
            color: Colors.white,
          ),
        ),
        const SizedBox(height: 5.0),
        Text(text, style: const TextStyle(fontSize: 12)),
      ]),
    );
  }

  Widget _emojiSelect() {
    return SizedBox(
      height: 250,
      child: EmojiPicker(
        config: Config(
            columns: 7,
            emojiSizeMax: 32 * (Platform.isIOS ? 1.30 : 1.0),
            verticalSpacing: 0,
            horizontalSpacing: 0,
            initCategory: Category.RECENT,
            bgColor: Color(0xFFF2F2F2),
            indicatorColor: Colors.blue,
            iconColor: Colors.grey,
            iconColorSelected: Colors.blue,
            progressIndicatorColor: Colors.blue,
            showRecentsTab: true,
            recentsLimit: 28,
            noRecentsText: "No Recents",
            noRecentsStyle:
                const TextStyle(fontSize: 20, color: Colors.black26),
            tabIndicatorAnimDuration: kTabScrollDuration,
            categoryIcons: const CategoryIcons(),
            buttonMode: ButtonMode.MATERIAL),
        onEmojiSelected: (category, emoji) {
          print(emoji);
          setState(() {
            _controller.text = _controller.text + emoji.emoji;
          });
        },
      ),
    );
  }
}
