// Code written by Oladeji Sanyaolu (24/9/2021)

import 'package:flutter/material.dart';
import 'package:dejchat/CustomUI/CustomUI.dart';
import 'package:dejchat/Model/Model.dart';

import 'Screens.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late ChatModel sourceChat;
  List<ChatModel> chatmodels = [
    ChatModel(
        name: "Dejloaf",
        isGroup: false,
        time: "4:00",
        currentMessage: "Hi Dejiiii",
        icon: "person.svg",
        id: 1),
    ChatModel(
        name: "Liz",
        isGroup: false,
        time: "3:58",
        currentMessage: "Hi Lizzz",
        icon: "person.svg",
        id: 2),
    ChatModel(
        name: "Carl",
        isGroup: false,
        time: "4:05",
        currentMessage: "Hey whatsup!",
        icon: "person.svg",
        id: 3),
    ChatModel(
        name: "Millie",
        isGroup: false,
        time: "4:05",
        currentMessage: "Hiiiiiiiii",
        icon: "person.svg",
        id: 4),
    // ChatModel(
    //     name: "Deji and the Gigachads",
    //     isGroup: true,
    //     time: "5:00",
    //     currentMessage: "Yoyoyoyoyoyoyo",
    //     icon: "groups.svg"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
          itemCount: chatmodels.length,
          itemBuilder: (context, index) => InkWell(
                onTap: () {
                  sourceChat = chatmodels.removeAt(index);
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (builder) => HomeScreen(
                        chatmodels: chatmodels,
                        sourcechat: sourceChat,
                      ),
                    ),
                  );
                },
                child: ButtonCard(
                  name: chatmodels[index].name,
                  icon: Icons.person,
                ),
              )),
    );
  }
}
