// Code written by Oladeji Sanyaolu (15/9/2021)

import 'package:flutter/material.dart';
import 'package:dejchat/Screens/Screens.dart';
import 'package:dejchat/Model/Model.dart';
import 'package:dejchat/CustomUI/CustomUI.dart';

class SelectContact extends StatefulWidget {
  const SelectContact({Key? key}) : super(key: key);

  @override
  _SelectContactState createState() => _SelectContactState();
}

class _SelectContactState extends State<SelectContact> {
  @override
  Widget build(BuildContext context) {
    List<ChatModel> contacts = [
      ChatModel(
        name: "Dejloaf",
        status: "A chad",
        isGroup: false,
        id: 1,
      ),
      ChatModel(
        name: "Liz",
        status: "Dejloaf's bestie",
        isGroup: false,
        id: 2,
      ),
      ChatModel(
        name: "Carl",
        status: "Marx",
        isGroup: false,
        id: 3,
      ),
      ChatModel(
        name: "E",
        status: "A Sports",
        isGroup: false,
        id: 4,
      ),
    ];
    return Scaffold(
      appBar: AppBar(
        title: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text("Select Contact",
                style: const TextStyle(
                  fontSize: 19,
                  fontWeight: FontWeight.bold,
                )),
            Text("256 contacts",
                style: const TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                )),
          ],
        ),
        actions: <Widget>[
          IconButton(icon: const Icon(Icons.search), onPressed: () {}),
          PopupMenuButton<String>(onSelected: (value) {
            print(value);
          }, itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(
                child: const Text("Invite a friend"),
                value: "Invite a friend",
              ),
              PopupMenuItem(
                child: const Text("Contacts"),
                value: "Contacts",
              ),
              PopupMenuItem(
                child: const Text("Refresh"),
                value: "Refresh",
              ),
              PopupMenuItem(
                child: const Text("Help"),
                value: "Help",
              ),
            ];
          }),
        ],
      ),
      body: ListView.builder(
          itemCount: contacts.length + 2,
          itemBuilder: (context, index) {
            if (index == 0) {
              return InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (builder) => CreateGroup()));
                },
                child: ButtonCard(icon: Icons.group, name: "New group"),
              );
            } else if (index == 1) {
              return ButtonCard(icon: Icons.person_add, name: "New contact");
            }
            return ContactCard(
              contact: contacts[index - 2],
            );
          }),
    );
  }
}
