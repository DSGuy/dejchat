// Code written by Oladeji Sanyaolu (11/9/2021)

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:dejchat/Config/Palette.dart';
import 'package:dejchat/Screens/Screens.dart';
//import 'package:dejchat/NewScreen/NewScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          fontFamily: "OpenSans",
          primaryColor: Palette.appPrimary,
          accentColor: Palette.appAccent),
      home: LoginScreen(), //LandingScreen(),
    );
  }
}
